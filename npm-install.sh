#!/usr/bin/env bash

# This script is called on npm install when used as a dependency.

# Halt on error - not in shebang for better Git Bash on Windows compatibility.
set -e

# Store current dir.
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# Go to dir where to install the git hooks.
cd "${INIT_CWD}"
# Install the git hooks.
. ${dir}/install-git-hooks.sh
