# install-git-hooks

Makes it easy for git hooks to be managed by version control.
Also takes care of some compatibility issues between Linux, Mac and Windows.

Requires (Git) bash.

##### Usage

From the root of your git project, run:

`bash git-hooks.sh`

This will create scripts for all the git hooks. These scripts look for `${hook_name}.sh` in the root of
your git project and run it. This way, your hooks will be subject to version control.

##### Use with NPM

You can also use this script with your NPM packages:

`npm install install-git-hooks --save-dev`

This will automatically install the git hooks.
A dev dependency will be added to `package.json` so every time a developer runs `npm install` for your
project, the git hooks will be installed automatically.
