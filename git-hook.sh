#!/usr/bin/env bash

# This script is called as a hook by git.

# Halt on error - not in shebang for better Git Bash on Windows compatibility.
set -e

# Get the hook's name from the script's filename.
hook_name=${0##*/}
# If a corresponding script exists in the project root, 'source' it.
if [ -f ${hook_name}.sh ]; then
    echo "Running ${hook_name} hook..."
    . ${hook_name}.sh
fi