#!/usr/bin/env bash

# This script installs script shortcuts for all git hooks
# so that the hook scripts will be subject to version control.

# Halt on error - not in shebang for better Git Bash on Windows compatibility.
set -e

# All git hooks. https://git-scm.com/docs/githooks/
hooks='applypatch-msg
pre-applypatch
post-applypatch
pre-commit
prepare-commit-msg
commit-msg
post-commit
pre-rebase
post-checkout
post-merge
pre-push
pre-receive
update
post-receive
post-update
push-to-checkout
pre-auto-gc
post-rewrite'

# Get the source for the git hook script relative to the current script.
git_hook_src_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/git-hook.sh"
# Make sure it's executable. (required by Git)
chmod u+x ${git_hook_src_path}

# Create hooks dir.
mkdir -p .git/hooks
# Create hooks by copying.
# It might also be possible to use symlinks instead...
for hook in ${hooks}; do
    git_hook_dest_path=".git/hooks/${hook}"
    cp -a ${git_hook_src_path} ${git_hook_dest_path}
done
